<?php
use Behat\Behat\Context\BehatContext,
    Behat\Behat\Event\SuiteEvent;
 
class FeatureContext extends Behat\MinkExtension\Context\MinkContext
{
    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        // Initialize your context here
    }
   /**
     * Pid for the web server
     *
     * @var int
     */
    private static $pids = array();
    private static $param;
    
    /**
     * Start up the web server
     *
     * @BeforeSuite
     */
    public static function setUp(SuiteEvent $event) {
        // Fetch config
        $params = $event->getContextParameters();        
        $url = parse_url(@$params['url']);
        $port = !empty($url['port']) ? $url['port'] : 80;
        
        self::$param = $params;
        
        // se l'host da dover analizzare è il 127.0.0.1 allora significa che 
        // si vuole far partire una istanza del server httpd interno di php
        // sulla index del progetto
        if (in_array(@$url['host'], array('127.0.0.1'))) {
            // si controlla che la porta indicata non sia già occupata
            if (self::canConnectTo($url['host'], $port)) {
                throw new RuntimeException('Something is already running on ' . @$params['url'] . '. Aborting tests.');
            }

            // viene fatto partire il web server di php sulla documentRoot speficicata (index)
            $pid = self::startBuiltInHttpd(
                $url['host'],
                $port,
                @$params['documentRoot']
            );
            // se non è partito ... errore
            if (!$pid) {
                throw new RuntimeException('Could not start the web server');
            }
            self::$pids[] = $pid;
        }
        // se viene indicato il parametro phantomPort allora bisogna far partire anche il server phantomJS
        if (@$params['phantomPort']) {
            // Try to start phantom
            if (self::canConnectTo('127.0.0.1', $params['phantomPort'])) {
                throw new RuntimeException('Something is already running on ' . @$params['phantomPort'] . ' (phantom port). Aborting tests.');
            }
            $pid = self::startBuiltInPhantom($params['phantomPort'], @$params['documentRoot']);
            if (!$pid) {
                throw new RuntimeException('Could not start the web server');
            }
            self::$pids[] = $pid;
        }
        
        $pids = self::$pids;
        
        // la registrazione di questa funzione permette di poter stoppare i 2 demoni quando behat 
        // termina l'esecuzione
        register_shutdown_function(function() use ($pids) {
            foreach($pids as $pid) {
                exec('kill ' . $pid);
            }
        });
        
        $start = microtime(true);
        $connected = false;

        // ora si rimane in attesa un po di tempo per vedere se i servizi avviati
        // rispondano alle chiamate oppure hanno avuto un problema di avvio
        if (in_array(@$url['host'], array('127.0.0.1'))) {
            // Try to connect until the time spent exceeds the timeout specified in the configuration
            while (microtime(true) - $start <= (int) @$params['timeout']) {
                if (self::canConnectTo(@$url['host'], $port)) {
                    $connected = true;
                    break;
                }
            }

            if (!$connected) {
                throw new RuntimeException(
                    sprintf(
                        'Could not connect to the web server within the given timeframe (%d second(s))',
                        $params['timeout']
                    )
                );
            }
        }
    }

    /**
     * Kill the httpd process if it has been started when the tests have finished
     *
     * @AfterSuite
     */
    public static function tearDown(SuiteEvent $event) {
    }


    /**
     * See if we can connect to the httpd
     *
     * @param string $host The hostname to connect to
     * @param int $port The port to use
     * @return boolean
     */
    private static function canConnectTo($host, $port) {
        // Disable error handler for now
        set_error_handler(function() { return true; });

        // Try to open a connection
        $sp = fsockopen($host, $port);

        // Restore the handler
        restore_error_handler();

        if ($sp === false) {
            return false;
        }

        fclose($sp);

        return true;
    }

    /**
     * Start the built in httpd
     *
     * @param string $host The hostname to use
     * @param int $port The port to use
     * @param string $documentRoot The document root
     * @return int Returns the PID of the httpd
     */
    private static function startBuiltInHttpd($host, $port, $documentRoot) {
        // Build the command
        /*
        $command = sprintf('php -S %s:%d -t %s >/dev/null 2>&1 & echo $!',
                            $host,
                            $port,
                            $documentRoot);
         * 
         */
        
        $dir = getcwd();
        var_dump($documentRoot);
        chdir($documentRoot);
        $command = sprintf("php -S %s:%d index.php > httpd.log 2>&1 & echo $!\n",
                            $host,
                            $port);
        $output = array();
        
        echo $command;
        exec($command, $output);

        chdir($dir);
        
        return (int) $output[0];
    }
    
    private static function startBuiltInPhantom($port, $documentRoot) {
        // Build the command
        /*
        $command = sprintf('php -S %s:%d -t %s >/dev/null 2>&1 & echo $!',
                            $host,
                            $port,
                            $documentRoot);
         * 
         */
        $command = sprintf("bin/phantomjs --webdriver=%s > phantom.log 2>&1 & echo $!\n",
                            $port);
        $dir = getcwd();
        chdir($documentRoot);
        
        $output = array();
        echo $command;
        exec($command, $output);
        
        chdir($dir);
        sleep(1);
        
        return (int) $output[0];
    }
//
// Place your definition and hook methods here:
//
//    /**
//     * @Given /^I have done something with "([^"]*)"$/
//     */
//    public function iHaveDoneSomethingWith($argument)
//    {
//        doSomethingWith($argument);
//    }
//
}
