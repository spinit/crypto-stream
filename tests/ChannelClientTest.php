<?php

namespace Spinit\CryptoStream\ChannelClientTest;

use Spinit\CryptoStream\ChannelClient;
use Spinit\CryptoStream\Base;
use Spinit\CryptoStream\Util;

ob_start();

class ChanneClientTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ChannelClient
     */
    private $object;
    
    public function setUp()
    {
        $dummy = new Base();
        $dummy->util(new TestUtil());
        $this->object = new ChannelClient();
        
        $this->urlbase = 'http://'.WEB_SERVER_HOST.':'.WEB_SERVER_PORT;
        $this->object->setPublicUrl("{$this->urlbase}/?pubkey");
    }
    
    public function testTransferData()
    {
        $data = 'ok';
        $actual = $this->object->getContent($this->urlbase.'/?echo=0', $data);
        $expected = sprintf('Ricevuto [%s]', $data);
        
        $this->assertEquals($expected, $actual);
    }
    
    public function testTransferDataCripted()
    {
        $this->object->enableCryption();
        $data = 'ok';
        $actual = $this->object->getContent($this->urlbase.'?echo=0', $data);
        $expected = sprintf('Ricevuto [%s]', $data);
        $this->assertEquals($expected, $actual);
    }
}

class TestUtil extends Util
{
    public function header($h)
    {
        return '';
    }
    public function session_start()
    {
        return '';
    }
}