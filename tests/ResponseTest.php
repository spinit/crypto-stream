<?php
namespace Spinit\CryptoStream\ResponseTest;

use Spinit\CryptoStream\Response;
use Spinit\CryptoStream\Util;

class ResponseTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Response
     */
    private $obj;
    
    public function setUp()
    {
        $this->obj = new Response('text/plain');
        $this->obj->util(new TestUtil());
    }
    public function testHeader()
    {
        $this->assertEquals('text/plain', $this->obj['Content-Type']);
        
        $this->obj->setHeader(array('key'=>'value'));
        $this->assertArrayHasKey('key', $this->obj);
        unset($this->obj['key']);
        $this->assertArrayNotHasKey('key', $this->obj);
        $this->obj['key'] = 'ok';
        $this->assertEquals('ok', $this->obj['key']);
    }
    public function testContent()
    {
        $this->obj->setBody('OK TEST');
        $this->assertEquals(0, count($this->obj->util()->headers));
        $this->assertEquals('OK TEST', (string) $this->obj);
        $this->assertArraySubset(array('Content-Type: text/plain'), $this->obj->util()->headers);
    }
}

class TestUtil extends Util
{
    public $headers = array();
    
    public function header($value)
    {
        $this->headers[] = $value;
    }
}