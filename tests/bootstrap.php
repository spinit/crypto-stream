<?php
include(__DIR__.'/../autoload.php');

if (!defined('WEB_SERVER_HOST')) {
    define('WEB_SERVER_HOST', '127.0.0.1');
    define('WEB_SERVER_PORT', rand(9000, 10000));
}
// Command that starts the built-in web server
$command = sprintf(
    'php -S %s:%d %s >%s 2>&1 & echo $!',
    WEB_SERVER_HOST,
    WEB_SERVER_PORT,
    dirname(__DIR__).'/html/index.php',
    '/dev/null' //dirname(__DIR__).'/html/php.log'
);

// Execute the command and store the process ID
$output = array(); 
exec($command, $output);
$pid = (int) $output[0];
echo $command;
echo sprintf(
    '%s - Web server started on %s:%d with PID %d', 
    date('r'),
    WEB_SERVER_HOST, 
    WEB_SERVER_PORT, 
    $pid
) . PHP_EOL;

// Kill the web server when the process ends
register_shutdown_function(function() use ($pid) {
    echo sprintf('%s - Killing process with ID %d', date('r'), $pid) . PHP_EOL;
    exec('kill ' . $pid);
});
