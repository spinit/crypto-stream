<?php
use Spinit\CryptoStream\Index;

class IndexTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var PDO
     */
    private $pdo;
    
    /**
     * @var Stream
     */
    private $obj;
    
    public function setUp()
    {
        /*
        $this->pdo = new PDO($GLOBALS['db_dsn'], $GLOBALS['db_username'], $GLOBALS['db_password']);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo->query("CREATE TABLE hello (what VARCHAR(50) NOT NULL)");
         * 
         */
    }
    public function tearDown()
    {
        if (!$this->pdo)
            return;
        $this->pdo->query("DROP TABLE hello");
    }
    public function testHelloWorld()
    {
        $helloWorld = new Index();
        $this->assertContains('Hello Word', $helloWorld->render());
    }
}