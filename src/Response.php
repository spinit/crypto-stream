<?php

namespace Spinit\CryptoStream;

/**
 * Description of Response
 *
 * @author ermanno.astolfi@spinit.it
 */
class Response extends Base implements \ArrayAccess
{
    private $headers = array();
    private $body = '';
    
    public function __construct($headers = '', $body = '')
    {
        if ($headers and !is_array($headers)) {
            $headers = array('Content-Type'=>$headers);
        }
        $this->setHeader($headers);
        $this->setBody($body);
    }
    
    public function setHeader($type, $value = '')
    {
        if (is_array($type)) {
            foreach($type as $k=>$v) {
                $this->setHeader($k, $v);
            }
        } else {
            $this->headers[$type] = $value;
        }
    }
    
    public function setBody($body)
    {
        $this->body = $body;
    }
    
    public function getBody()
    {
        return $this->body;
    }
    
    public function __toString()
    {
        foreach($this->headers as $k => $v) {
            $this->util()->header($k.': '.$v);
        }
        return $this->getBody();
    }
    
    public function offsetExists($offset)
    {
        return isset($this->headers[$offset]);
    }
    
    public function offsetUnset($offset)
    {
        unset($this->headers[$offset]);
    }
    
    public function offsetGet($offset)
    {
        return $this->headers[$offset];
    }
    
    public function offsetSet($offset, $value)
    {
        $this->headers[$offset] = $value;
    }
}
