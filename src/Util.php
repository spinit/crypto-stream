<?php
namespace Spinit\CryptoStream;


\date_default_timezone_set('Europe/Rome');

class Util
{
    private static $logFile = '';
    
    static function setLogFile($file)
    {
        self::$logFile = $file;
    }
    static function log($title)
    {
        if (!self::$logFile) {
            return;
        }
        $args = func_get_args();
        array_shift($args);
        if (count($args)>1) {
            $msg = $args;
        } else {
            $msg = array_shift($args);
        }
        $fp = fopen(self::$logFile,'a');
        if (!$fp) {
            return;
        }
        chmod(self::$logFile, 0666);
        $root = realpath(__DIR__.'/..');
        $debug = debug_backtrace();
        $file = substr($debug[0]['file'], strlen($root));
        $line = $debug[0]['line'];
        $content = date("Ymd H:i:s")." ".getmypid()." : {$file}:{$line} >> $title";
        if ($msg) {
            if (!is_string($msg)) {
                $msg = print_r($msg, 1);
            }
            $content .= "\n".$msg;
        }
        fwrite($fp, $content."\n");
        fclose($fp);
    }
    
    /**
    * Decrypt data from a CryptoJS json encoding string
    *
    * @param mixed $passphrase
    * @param mixed $jsonString
    * @return mixed
    */
    function decrypt($passphrase, $jsonString){
        $jsondata = json_decode($jsonString, true);
        $salt = hex2bin($jsondata["s"]);
        $ct = base64_decode($jsondata["ct"]);
        $iv  = hex2bin($jsondata["iv"]);
        $concatedPassphrase = $passphrase.$salt;
        $md5 = array();
        $md5[0] = md5($concatedPassphrase, true);
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
            $result .= $md5[$i];
        }
        $key = substr($result, 0, 32);
        $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
        return json_decode($data, true);
    }

    /**
    * Encrypt value to a cryptojs compatiable json encoding string
    *
    * @param mixed $passphrase
    * @param mixed $value
    * @return string
    */
    function encrypt($passphrase, $value){
        $salt = openssl_random_pseudo_bytes(8);
        $salted = '';
        $dx = '';
        while (strlen($salted) < 48) {
            $dx = md5($dx.$passphrase.$salt, true);
            $salted .= $dx;
        }
        $key = substr($salted, 0, 32);
        $iv  = substr($salted, 32,16);
        $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
        $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
        return json_encode($data);
    }
    
    public function __call($name, $arguments) {
        return call_user_func_array($name, $arguments);
    }
    
    public function array_get($array, $field, $default = '')
    {
        if (isset($array[$field])) {
            return $array[$field];
        }
        return $default;
    }
}
