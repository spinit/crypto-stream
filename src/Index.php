<?php

namespace Spinit\CryptoStream;

class Index
{
    public function __construct()
    {
    }
    public function render()
    {
        return file_get_contents(implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__) , 'asset', 'index.html')));
    }
}
