<?php
namespace Spinit\CryptoStream;

interface RestoreInterface
{
    public function loadObject();
    public function storeObject($data);
}
