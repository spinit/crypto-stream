<?php

namespace Spinit\CryptoStream;

class JsLib
{
    /**
     * Directory radice della libreria
     * @var type 
     */
    private $workdir;
    
    public function __construct()
    {
        $this->workdir = dirname(__DIR__);
    }
    public function __toString()
    {
        header('Content-Type: application/json');
        readfile($this->workdir.'/asset/js/crypto-js.js');
        readfile($this->workdir.'/asset/js/jsencrypt.js');
        include ($this->workdir.'/asset/js/stream.php');
    }
}
